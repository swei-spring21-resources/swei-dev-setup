# Software Engineering I Spring 2021 Student Developer Wiki

## Table of Contents

- [Software Engineering I Developer Setup Guide](#markdown-header-software-engineering-i-spring-2021-student-developer-setup-guide)
  - [Software Engineering I Software Suite](#markdown-header-software-engineering-i-software-suite)
    - [Slack Board](#markdown-header-slack-board)
    - [Bitbucket Student Account](#markdown-header-bitbucket-student-accounts)
  - [Software Installs](#markdown-header-software-installs)
    - [Python 3](#markdown-header-python-3)
    - [Git](#markdown-header-git)
  - [IDE Setup](#markdown-header-ide-setup)
    - [PyCharm](#markdown-header-pycharm-setup)
    - [IntelliJ](#markdown-header-intellij-idea-setup)
    - [Microsoft Visual Studio Code Setup](#markdown-header-microsoft-visual-studio-code-setup)
  - [Python and VCS Bootcamp Summary](#markdown-header-bootcamp-summary)
    - [Python Programming Review](#markdown-header-python-object-oriented-programming-and-unittest-module)
    - [Bitbucket and Version Control Documentation](#markdown-header-version-control-module)
  - [Team role delegation](#markdown-header-team-role-delegation-swe-spikes-and-activities)

## Software Engineering I Software Suite

### Slack Board

[Slack](https://slack.com/) is a popular business communication application that we'll use in this course as a more direct form of communication. Due to the fully-online structure of this course, it's imperative that you join our slack channel. The TA's will create Slack channels for each team, respond to any questions you might have, and post helpful resources and updates on general channels.

The TA's should have sent you an invite to your team's slack channel in the Software Engineering 2021 workspace.

Creating a slack account is simple,

1. [Sign up](https://slack.com/get-started#/create) for a free account using your St. Edward's Google account.
2. I highly recommend downloading and installing the desktop app on your system after you've been added to our channel.
   MacOS Download [Link](https://slack.com/downloads/mac)
   Windows Download [Link](https://slack.com/downloads/windows)
3. If you weren't added to the software engineering workspace, try using this link after creating your slack account: https://join.slack.com/t/2021softwaree-zn82715/shared_invite/zt-jdtrx83n-PAe6_RKwc~4gbTgqdbQifQ

Contact the TA's if you're weren't added to the Software Engineering '21 workspace.

**Tip:**  
I suggest each team use Slack (or another form of virtual communication) to do your [daily standups](https://www.atlassian.com/agile/scrum/standups). Standups can be sent through chat formatted in such a way,

```
Geoff
Y (stands for what I did (Y)esterday): Wrote a python class for Order
T (stands for what I'm doing (T)oday):  Writing unit tests for the Order class
B (stands for (B)lockers): None
```

### Bitbucket Student Accounts

[Bitbucket](https://bitbucket.org/product) is a software version control repository service owned by Atlassian. In this course, you and your team will use Bitbucket repositories to collaboratively write the source code involved in your project. Each student is **required** to sign up for a free Bitbucket student account.

1. Use this [link](https://bitbucket.org/product/education) to sign up for a Bitbucket student account. Make sure that you sign up using your St. Edward's google account or email.
2. You should receive an email confirmation that your account was successfully created.
3. Once your team has all signed up for Bitbucket student accounts, the TA's will follow up by creating the private Bitbucket workspace for your team which you'll use throughout the semester.

After step 3 you should get some type of email invitation with a link to your team's workspace. Contact the TA's if you have any issues creating an account.

## Software Installs:

### Python 3

Unless you have a good reason not to, it's generally a good idea to install the most recent version of software releases. For this course, i'd recommend that you have at least version **3.8**
Here are the links for installing python (if you don't already have it installed):

- [MacOS](https://www.python.org/downloads/)
- [Windows](https://www.python.org/downloads/windows/)
- [Linux](https://www.python.org/downloads/source/)

_When installing with Windows, I suggest that you install Python globally (to all users) and make sure that the option "Add Python to System PATH variable" is checked_

To make sure that the Python library and interpreter installed propertly, open up a new terminal, command prompt, or PowerShell window. Type `python3 -V` which will output your version. You can also type `python3` which will open an interactive python3 shell, giving output on your version.

### Git

Git is a version-control system for tracking changes to a repository of source code files. Git is designed for handling large software projects in team environments with multiple developers. In this course we'll learn to use version control systems through Git, so you'll need to install it onto your system.

Here is the [link](https://git-scm.com/downloads) to install Git on Windows, Linux, and MacOS. You can use the command `git --version` to make sure that the `git` library installed properly.

## IDE Setup

### Back-end

When choosing an Integrated Development Environment for this course, there are a few things you'll want to consider:

- Python Support and plugin options
- Git Integration

Both JetBrains [PyCharm](https://www.jetbrains.com/pycharm/) and [IntelliJ IDEA](https://www.jetbrains.com/idea/) are great choices for your IDE in this course. Both are intuitive to use and have integrated terminals where you can run easily run commands.

Keep in mind that you can create a JetBrains account for free with your student email using this [link](https://www.jetbrains.com/community/education/#students)

### PyCharm Setup

- Download and install the PyCharm Community IDE application using this [link](https://www.jetbrains.com/pycharm/download/#section=windows). Note that there are different versions of the MacOS installer depending on your system hardware which you can find in the `About this Mac` desktop menu.

### Installing Plugins

Open up a new PyCharm window and click on the `Plugins` option on the left toolbar. You should see a search bar and a list of plugins you can install with your IDE. Here are a few i've found useful:

- GitToolBox
- Code With Me
- .ignore
- CodeStream

![PyCharm Plugin Screenshot](https://i.imgur.com/buBcqXA.png)

### Unit testing in PyCharm

Unit tests in PyCharm are simple to create and run with PyCharm.

By default, when you create a new unit test file, the `unittest` library will be used. This will be fine for this course, but if you want to use another unit testing library, you can configure this in your PyCharm preferences.

- Navigate to `Preferences --> Tools --> Python Integrated Tools`
- You should see a dropdown menu titled `Testing`. There are different unit testing libraries you can select here which will be used any time you click on `Create new file` and select the `Python unit test` option.

To quickly create a new unit test for your Python OO Classes

- Go to the Class file.
- Right click on the class name inside the file.
- Select the `Go To` option, the the `Test` option.
- A new popup will appear where you can select the test methods which will be built by PyCharm in a new test file.

By default, all of your unit tests will fail. You can run the unit test file using the Green play button on the IDE. to the left of the unit test class definition.

![PyCharm Class Unittest preview](https://i.imgur.com/JVrq6v7.png)

![PyCharm unit test selection window](https://i.imgur.com/cf562p9.png)

### Debugging in PyCharm

In reality, we won't always pass every test case we write. Debugging a failed test case in PyCharm is simple.

- Place a breakpoint on the line of the failed test. You can do this by clicking just to the right of the line number on the IDE. There should be a red circle Icon appear.
- Click on the Debug button next to the green play button on the top-right corner of the window.
- You should see output where the instruction pointer stopped, or the state of your tests on your debugger console.

![PyCharm Debugging Screenshot](https://i.imgur.com/VEFmOQX.png)

### PyCharm Shortcuts and Helpful Links

Here's some additional helpful links for PyCharm shortcuts and documentation.

- [PyCharm Shortcuts](https://www.jetbrains.com/help/pycharm/mastering-keyboard-shortcuts.html#advanced-features)
- [PyCharm Command line launcher](https://www.jetbrains.com/help/pycharm/working-with-the-ide-features-from-command-line.html#standalone)

### IntelliJ IDEA Setup

- Download the IntelliJ IDEA IDE using this [link](https://www.jetbrains.com/idea/download/#section=windows)
- Complete the Installer (you can skip the launcher script, tasks, and featured plugin sections for now). Now you should see the IntelliJ new project window on your screen:

![IntelliJ IDEA New Project](https://i.imgur.com/h1ePAt3.png)

### Installing Plugins

- Click on the plugins option. We'll need to install the Python plugin.
- Click on Marketplace in the Plugins window. Search for "python". Then install the Python Community Edition plugin and restart IntelliJ.

![Python Search in Marketplace](https://i.imgur.com/ooIk1vH.png)

- Click the "Create new project" button when your IDE reloads. You should see an option now to create a new Python project.

![Create New Python Project](https://i.imgur.com/6ds84CO.png)

- Make sure that the Project SDK is set before creating the project and that you've installed Python. IntelliJ should be able to auto-detect where the Python library was installed on your system.

**Tip**  
Here are some additional plugins you can install from the IntelliJ Marketplace which i've found useful:

- GitToolBox
- Rainbow Brackets
- .ignore
- File Watchers

## Front-end

### Microsoft Visual Studio Code Setup

For any type of front-end development I highly suggest using Microsoft Visual Studio Code. VS Code is a powerful editor and is widely-used in the industry for front-end development.

Download the Visual Studio Code Editor using this [link](https://code.visualstudio.com/) and install it. If you're using Windows, I suggest checking the option "Add to PATH" Which will let you use the command line launcher feature described below.

**Tips**  
If you're using a MacOS system, I recommend setting up a command line launcher with the VS Code app so that you can easily launch vscode inside a source folder by issuing the command `code .`
[Here](https://code.visualstudio.com/docs/setup/mac) is some documentation to set this up on MacOS.

Here are a few extensions on VS code which i've found useful:

- Debugger for Chrome
- Git Extension Pack
- Bookmarks
- TODO Tree
- PlantUML
- Code Runner

Here's a quick [video](https://www.youtube.com/watch?v=xkwJ9GwgZJU&t=286s&ab_channel=ChristopherFuhrman) on how to use Visual Studio code to write (and export) PlantUML documents for your sequence diagrams

## Bootcamp Summary

The python and version control bootcamp module linked below will provide a smoother technical transition for you and your team members, as there is likely a lot of new technical concepts you may not be familiar with.

_Trello Card Text Suggestion: 1.04 (XL) As a software engineer, I will complete the Python OOP, Unit test and Version Control Module and present my findings to my team so that my team and I are more prepared to manage our code repositories in Sprint 1 and beyond._

### Python Object Oriented Programming and Unittest Module

See Python OOP and Unittest [module](oop/PyOOPAndUnittest.md)

### Version Control Documentation

See version control [documentation](vcs/vcs.md)

## Team role delegation: SWE Spikes and Activities

Each team will have subject matter experts on their team who will specialize in a certain subfield of software engineering in this course.
For our Multi-cloud TaaS solution in this course specifically, 5-member teams are traditionally split up as such,

```
Team 11
Geoff: DevOps Engineer
Ruben: Mapping Services
Ethan: Demand-Cloud Full-Stack Engineer
Martha: Supply-Cloud Full-Stack Engineer
Shelby: Scrum Master
```

Below are links with activities and documentation to help guide you in your collaborative decisions. Use the activities in each learning module as previews into the types of tasks you'll experience in this course.

Make sure to keep your **team documemtation in your Google Drive knowledge repositories up to date.** Maintaining your own team documentation will help spread knowledge transfer among team members and provide a better software presentation come demo day. You can use these activities as a starting point for writing your own documentation.

## Interested in the Full-stack Engineer Role?

See REST [documentation](rest/rest.md)

## Interested in the DevOps Engineer Role?

See DevOps [documentation](devops/devops.md)

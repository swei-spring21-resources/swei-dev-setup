# DigitalOcean Back-end Configuration

[DevOps Home](../devops.md)

[Wiki Home](../../README.md)

## Python Installation

Another technology stack requirement of the popular LEMP stack is Python. Python 3 is pre-installed automatically on DigitalOcean Ubuntu v20.04 images, so the only prerequisite to configuring Python3 on our droplet is to have a user with `sudo` privileges.

First, update and upgrade your package manager:
`sudo apt update && sudo apt upgrade`

You can check that Python 3 is installed on your droplet by running the command,

- `python3 -V`

Make sure that your output states a version of Python higher than Python 3.7.

Next, let's install `pip`, a Python package manager which will install and manage the Python packages we want to use in our projects.

- `sudo apt install --yes python3-pip`

You can install packages using the syntax: `pip3 install package_name`

There are a few more development tools we should install on our Droplet to make sure that our Python environment is robust,

- `sudo apt install -y build-essential libssl-dev libffi-dev python3-dev`

### Setting up a Python Virtual Environment

[Python Virtual environments](https://docs.python.org/3/tutorial/venv.html) are a way to isolate your Python projects such that the dependencies of your specific Python project don't interfere with any other Python projects you may have. Virtual environments consist of a specific Python interpreter, a directory tree for the modules needed to run the Python interpreter of your choice, and a myriad of other packages. Simply, you can think of each virtual environment as a container running on our server which will manage our Python environment.

We'll setup our Python Virtual environment using the `venv` module. First, we need to install it.

- `sudo apt install --yes python3-venv`

Now we can create our virtual environment. Replace `<your_environment_name_here>` with the name you want.

```
cd ~
mkdir environments
cd environments

# Create the new virtual environment inside an environments directory.
python3 -m venv <your_environment_name_here>
```

You can activate your Python virtual environment by running the activate script,

- `source <your_environment_name_here>/bin/activate`

You should now see a prefix on your command prompt with the name of the Python virtual environment you configured earlier.

`(your_environment_name_here) geoff@ubuntu:~/environment$: `

Instead of having to remember to run this command everytime we need to update Python packages on the server, we can simply add the `source` activation line of code above to our `.profile` file.

In Linux, there are certain files which are executed as login scripts. One of these files, by default in Ubuntu, is `.profile`, which can be found in your home directory,

- `cat ~/.profile`

If we add the

- `source <your_environment_name_here>/bin/activate`

activation line of code the `.profile` script, everytime we login (as the user who we're currently logged in as), our Python virtual environment will already be loaded.

Now try adding the line of code above to your `.profile` script. Then log out, log back in, and check the see if your Python virtual environment was activated upon user login to the droplet.

## Git Configuration

Make sure that Git is installed on your droplet first by issuing the command,
`git --version`

Next, let's create a new directory where we'll put our source code on our droplet.

```
cd /home
sudo mkdir building-risk-management

# Assign the directory the correct ownership (by default, root ownership is assigned to a directory created with 'sudo'). Replace 'geoff' with your user you setup # earlier.
sudo chown -R <your_user>:<your_user> building-risk-management

# clone the source code repository for my senior project
cd building-risk-management
git clone https://github.com/GeoffA12/building-risk-management-back-end.git
```

_Tip_:
Right now, only we have access to maintain this code repository on our droplet. Eventually, your teammates may want to help configure your DigitalOcean Droplet. Here's how to go about this,

- Tell your teammate to create their own SSH Key pair on their computer.
- Have your teammate send you a copy of their public key.
- Login as the root user and create your teammate's new account and setup their `authorized_keys` file using the [devops documentation](../devops.md)
- Add a new linux user group. Groups are a way to assign permissions levels to multiple user in Linux. Look at this [link](https://www.techrepublic.com/article/how-to-create-users-and-groups-in-linux-from-the-command-line/) on how to do this.
- Add both yourself and your teammate's user account to the new group.
- Assign ownership of the code repository using the `chown` command to the group you created.

## Systemd configuration (Very Important!)

Systemd [link](https://www.digitalocean.com/community/tutorials/understanding-systemd-units-and-unit-files) used to help write this section

Systemd is a Linux process manager which is responsible for controlling which `units` execute when the system is turned on. A `unit` in Linux is a system resource that can be managed by a list of [daemon threads](https://www.quora.com/What-is-daemon). Units include network resources, filesystem mounts, and isolated resource pools.

![Linux Systemd Preview](https://i.imgur.com/7RTlRI7.png)

Many modern Linux distributions like Ubuntu have completely shifted management of system units, such as `ssh.service` to the systemd process manager. One of the main reasons for this was that previous Linux process managers grouped together chunks of system units which made it difficult to manage individual units without modifying the core behavior of a unit.

**Systemd unit service files** define how `systemd` will handle a Linux unit and are found in multiple locations in the Ubuntu operating system. Service files describe to the Ubuntu operating system how a service should be stopped or started, and under which circumstances the service should automatically be restarted, as well as any system dependencies for the service.

The path that we care about is `/etc/systemd/system`, which is where we need to define our own service file for our back-end application server which will `listen` and `respond` to HTTPS requests from clients on a specified port as a _background_ process. Service files for units found in the directory `/etc/systemd/system` take precedence over any of the other locations on the file system, and this is where we'll write our own service file.

Before we write the service file, we need to do some pre-configuration on our Java application we cloned earlier.

### Setup a systemd user

First, we'll add an extra layer of security by creating a user who will _only_ be granted permissions to execute our own custom systemd service file.

```
# Use any password you want here. Skip the full name, and other details you'll be prompted with when you create this new user.
sudo adduser systemduser
```

Next, we need to assign permissions to the executable file `brm.jar` included in the code repository we cloned earlier. This `executable` path is needed in the systemd service file we'll later write, so make note of it:

Install Maven. Then bulid the application.

- `sudo apt install maven`
- `cd /home/building-risk-management/building-risk-management-back-end`
- `git pull && sudo mvn clean install`

You should see a `BUILD SUCCESS` message on your terminal. Next, grant the `systemduser` read and execute permissions to the `brm.jar` executable

- `cd target`
- `sudo chown systemduser:systemduser brm.jar`
- `sudo chmod 500 brm.jar`

### Writing a systemd service file

```
cd /etc/systemd/system
sudo nano brm.service
```

Copy and paste the contents below into the `brm.service` file.

```
[Unit]
Description=Building Risk Management Application
After=syslog.target

[Service]
User=systemduser
ExecStart=/home/building-risk-management/building-risk-management-back-end/target/brm.jar
Restart=always
RestartSec=3

[Install]
WantedBy=multi-user.target
```

**Description:**
As you can see above, service files are organized in sections.

### Unit block section

- Generally used for defining metadata for the unit and any relationships the unit (our back-end application, in this case) has to other units.
- `Description`: Gives a name to describe the functionality of the unit
- `After`: The Units in this directive will be started before the current unit. In this case, we're stating that we want a system log output after the application is stopped.
- _Tip_: Start researching the `Requires` directive. This is an additional directive you may need to add depending on which database solution you pick for your TaaS application. The `Requires` directive will list any units which your unit depends on, which might be a database, for instance.

### Service block section

- This section provides configuration that is only applicable for a service.
- Notice that the `User` directive is set to the `systemduser` we created earlier. This is because this user has the correct permissions to execute the `brm.jar` file located at the path directive given in `ExecStart`.
- `ExecStart` specifies the full path and any optional arguments of the command to be executed which will start the process.
- By default, when a `Type` directive is not given in the service block section, but an `ExecStart` directive is given, Linux will assume that we're a `Simple` process. You should read this [thread](https://superuser.com/questions/1274901/systemd-forking-vs-simple/1274913) to learn more about the different types of processes in Linux, as you may not want to run a `Simple` process for your team's application.

### Install block section

- This is an optional section and defines the behavior of a unit if it's enabled or disabled. Remember that enabling a unit means that we're telling the Ubuntu systemd manager to start the process on system boot. Conversely, disabling a unit will disabled the unit from starting on system boot.
- Only units that can be enabled should include this section. We want to persist our application even if our droplet is accidentally restarted or powered off, so we need to include a directive here.
- `WantedBy` directive specifies how a unit should be enabled.

Make sure that you test out your service file before proceeding to the next step. Save the `brm.service` file.

```
sudo systemctl start brm.service
sudo systemctl enable brm.service

# Check the status to make sure the service is currently running
sudo systemctl status brm.service
```

Your output should look something like this after checking the status:

```
brm.service - Building Risk Management Application
     Loaded: loaded (/etc/systemd/system/brm.service; enabled; vendor preset: enabled)
     Active: active (running) since Sun 2021-01-17 05:48:49 UTC; 12s ago
   Main PID: 146107 (brm.jar)
      Tasks: 17 (limit: 1137)
     Memory: 115.2M
     CGroup: /system.slice/brm.service
             ├─146107 /bin/bash /home/building-risk-management/building-risk-management-back-end/target/brm.jar
             └─146131 /usr/bin/java -Dsun.misc.URLClassPath.disableJarChecking=true -jar /home/building-risk-management/building-risk-management-back-end>
```

# DevOps Module

[Back to Wiki Home](../README.md)

DevOps stands for Development and Operations and is a software engineering practice which indeed bridges a gap between an organizations IT staff and software developers. DevOps engineers are more focused on stabilizing the deployment environment of software rather than the business logic or implementation of the code itself. _In general_, DevOps engineers work with software teams and IT departments to minimize software release failures and minimize the time required to fix software bugs in a new release. [Here's](https://medium.com/edureka/devops-engineer-role-481567822e06#:~:text=DevOps%20Engineer%20works%20with%20developers,planning%20of%20test%20and%20deployment.) a nice article detailing more about the different career types of DevOps engineers, the tools they commonly need to use, and so on.

In this module we'll take a look at creating a stable and fully-functional cloud environment so that you're more prepared to deploy your team's work during Sprint 2 by configuring a test DigitialOcean droplet to support my Senior Project.

You should follow every step here. Make sure to read through the additional documentation on the bottom of this guide for setting up MongoDB, Java, and Nginx, which will all be needed to deploy my senior project.

## Software Versions:

- nginx: 1.18.0 (Ubuntu)
- Ubuntu: 20.04.2 LTS (GNU/Linux 5.4.0-51-generic x86_64)
- MongoDB: 4.4.3
- Python: 3.8.5
- certbot: 0.40.0
- git: 2.25.1

Program versions can be checked by calling `[program-name] --version`, or if that doesn't return it, `[program-name] -v`.

## Learning Resources

- [Linux File Permissions](https://www.pluralsight.com/blog/it-ops/linux-file-permissions)
- [Bash Scripting Cheatsheet](https://devhints.io/bash)
- [SSH Configuration File](https://linuxize.com/post/using-the-ssh-config-file/)
- [Linux File System](https://www.youtube.com/watch?v=HbgzrKJvDRw&ab_channel=DorianDotSlash)

## Important Linux Commands and Definitions

- `~` : This character refers to the current signed-in user's home directory on Linux/UNIX machines (e.g. if the current signed in user is `team23`, `~` is the same as `/home/team23`). This also works on Windows using Powershell, but not the Command Prompt (`cmd.exe`), and is recognixed on Macs. All of our servers for this course use Ubuntu (a Linux distribution) so the `~` is a valid way to get the user's home directory. In Powershell, `~` refers to your user home directory, which is `C:\Users\[your-username]`.
- `pwd` : pwd is the UNIX command for "present working directory". Calling this command will print out where you currently are in the system (useful if you think a file or folder should be there but you aren't sure where you are).
- `mv [source-file-or-directory] [destination-file-or-directory]` : mv is the unix command for "move". It takes a source and a destination, and is also used to rename a file. Passing just a file name or directory name wihout a full path will implicitly use the current path that the terminal is at (which can be shown with `pwd`).
- `cd [directory]` : cd is the unix command for "change directory". This changes the current directory your terminal is looking at to the directory specified. This command can take an absolute path which starts with either `/` or `~` (e.g. `cd /etc/nginx/` or `~/.ssh/`) or can take a path _relative to the directory you are currently in_ (e.g. you are in a directory with a subdirectory called "supply", you can say `cd supply` and you will descend into that folder).
- `sudo` : sudo stands for "switch user do". This command allows for executing single commands as a user with elevated permissions on the system, without being logged into that elevated account all the time.
- `apt` : The package manager on Ubuntu. This is the command for installing/removing/updating software and applications on Ubuntu.
- `ls` : This command will list all files or directories. Use the `-a` option to list all files incluing hidden files. Use the `-l` option to show long list of files which includes more details such as file permissions, ownership, size, and modification date.

## SSH into Server as Root and Create a new User

SSH stands for secure shell and is a network authentication protocol commonly used for client-server models. DigitalOcean allows us to configure SSH authentication upon creation of a new droplet. This should already be done for you. Contact one of the TA's on slack to receive your Root user SSH key pair and domain (or IP address) to login to your test droplet for this module.

SSH tutorials are defined below, based on your operating system. If you are using a recent version of Windows 10 (1803 or later, checkable in Settings>System>About this PC), your machine already has OpenSSH installed by default. If you are on an older version of Windows, see the section on using PuTTY below.

### Using OpenSSH (MacOS, Windows 10 Version 1803+, Linux)

Digital Ocean handled adding the key to the droplet for us, so we are good to connect in using the key, our `root` user. The ssh command should be formatted as follows,

- MacOS: `Terminal.app`, found in Applications/ folder
- Windows: `Powershell`, openable using `Windows + R` keys on the keyboard, and typing 'powershell' in the dialog box that appears and hitting enter
- Linux: Your terminal emulator of choice (ensure that you have ssh packages installed, if you'ure using a Linux distribution as your primary OS then you should be able to do this)

```
ssh -i ~/.ssh/id_rsa root@[domain-name-or-ip]
```

On your first SSH connection, the system will ask if you'd like to accept the authenticity of the host. Say yes to this

_Reminder: The key generated above defaults to_ `id_rsa`_, however if you are using a different keypath on your machine substitude in that path for_ `~/.ssh/id_rsa`.

### Using PuTTY

This should be fairly simple, although you'll need to do an additional step of converting the OpenSSH private key generated by DigitalOcean to a PuTTY compatible key (which has a .ppk extension). OpenSSH key pairs are also generated by default when using the `ssh-keygen` command and aren't automatically compatible with PuTTY.

To Convert your OpenSSH private key to a PuTTY private key,

- Open up Puttygen.exe
- Click on `Conversions`, and then `Import`.
- Select your OpenSSH private key (e.g., "C:\Users\Geoff\.ssh\id_rsa")
- There shouldn't be a passphrase you need to fill out. You can leave the passphrase input fields empty.
- Click on the `Save private key` button.

![OpenSSH to PuTTY conversion](https://i.imgur.com/zypUTLu.png)

To login to your DigitalOcean Droplet using your PuTTY key,

- Open up Putty.exe
- Enter your remote hostname IP Address or domain name
- Leave the default port as is (Port 22)
- Navigate to `Connection --> SSH --> Auth`
- Click on `Browse...` under `Authentication parameters / Private key file for authentication`
- Locate your `.ppk` private key file you just saved.
- Click on `Open` to log into your remote server with your private PuTTY key.

![Logging in with PuTTY](https://i.imgur.com/bpfRlQD.png)

## Creating a new System User (With SSH password authentication disabled)

In general, it's not good practice to use the `root` user login unless we need to perform system-wide updates like adding new user accounts. This has to do with the `Principle of least privilege` in computer security. As root users have the highest authorization level in the system, if a security vulnerability were to be exploited, damages could be insurmountable. Security breaches though which an attacker gains access to our root credentials could result in us completely losing access to our cloud production environment.

You should setup a new user for yourself.

- Login as the `root` user
  Follow these commands:

* `adduser <your_user>`

Replace `<your_user>` with your name. You'll be prompted to enter a new password for this user. Write this down or remember it as you'll need it later. You can skip all of the other details like Full Name and Work Phone.

Verify your user was created using:

- `grep <your_user> /etc/passwd`

Next, add your new user to the `sudo` group so that you can use the `sudo` command to gain temporary privileged access. By default, all members of the `sudo` group in Ubuntu have `sudo` privileges.

- `usermod -aG sudo <your_user>`

Now logout and create a new OpenSSH (or PuTTY if you don't have access to OpenSSH) key pair.

- Open up a Terminal or PowerShell window.
  To create the key, use the command,
- `ssh-keygen -t rsa -b 4096`

The `-t` option tells the key generator which algorithm to use. The `-b` option is the byte size of the key pair.

- Skip the passphrase by just hitting enter on the terminal.

Make sure you remember where the private/public key was saved on your system. You should get a message from `ssh-keygen` confirming which directory you want to store your key pair and what you want to name the key pair. You can change the directory of where the key-pair will be put if you want to.

For this example, i'll assume the private key I just created is named `your_user_rsa` in the directory `C:\Users\Geoff\.ssh`

- Copy the contents of the `your_user_rsa.pub`, which is your public key you just created, **likely in the `.ssh` directory by default.**

Since password authentication is disabled on our DigitalOcean droplet, we need to log back in as the `root` user and add a `~/.ssh/authorized_keys` file to our new user. The `authorized_keys` file is a special file which the ssh protocol uses to authenticate the key pair of the user trying to login, and as of now, it doesn't exist in the home directory of the user we setup earlier.

First copy the contents of your new public key, `your_user_rsa.pub`. You'll need to paste this in the `authorized_keys` file later.

```
# Login to the server as root (remember to use your first private key which was given to you, and not the new private key you just created)
ssh -i id_rsa root@[ipaddress or domainname]

# Switch user context to the new user's account
su - <your_user>

# Create the .ssh directory and assign it the correct permissions. Replace 'geoff' with the name of the user you created.
cd ~
sudo mkdir .ssh
sudo chown -R <your_user>:<your_user> .ssh
sudo chmod 700 .ssh

# Create the authorized_keys file, assign it the correct permissions, and copy your public key geoff_rsa.pub into the file. Then save the #file.
cd ~/.ssh
nano authorized_keys
(copy and paste your public key for your new user..)

# Once the authorized_keys file has been created and edited with your new private key, you can use the 'exit' command to logout.
```

Log off the server. Now try logging in as your new user, `<your_user>`. Remember to use the new private key you created earlier with the `ssh-keygen` command.
`ssh -i your_user_rsa your_user@[ipaddress or domainname]`.

Continue with the rest of this module once your able to login to the droplet with your user account and not the root account.

## MongoDB Installation and Configuration

- [MongoDB Docs](mongo/mongo.md)

## Python, Git, and Systemd Configuration

- [Backend Docs](backend/backend.md)

## Nginx Server Configuration

- [Nginx Docs](nginx/nginx.md)

## MySQL Installation and Configuration

- [MySQL Docs](mysql/mysql.md)

# MongoDB Installation and Configuration

[DevOps Home](../devops.md)

[Wiki Home](../../README.md)

[MongoDB](https://www.mongodb.com/) is a very powerful and common NoSQL Database Solution. NoSQL database are typically used in real-world situations where we need to persist and read [Big Data](https://www.mongodb.com/big-data-explained#:~:text=Big%20Data%20refers%20to%20very,%2C%20webpages%2C%20and%20multimedia%20content.) distributed across multiple database servers, often called clusters. The technique of splitting up data across multiple database servers in different regions around the world is known as [Database Sharding](https://docs.mongodb.com/manual/sharding/). Typically, sharding is easier to implement with NoSQL databases like MongoDB than relational databases like MySQL. MongoDB is also a highly-available (99.995%) solution and offers clients an easy way to store non-traditional formats of data unlike strings or numbers and like images or movies.

![MongoDB Sharding](https://i.imgur.com/aNYoVTV.png)

NoSQL databases don't follow the row and column format of persisting data like relational databases such as MySQL. This gives database designers more flexibility when designing applications, as there's no need to write and define a schema or table prior to configuring new documents. Non-relational databases typically persist data in a `key-value` pair or in a JSON or XML `document`.

![MongoDB Document](https://i.imgur.com/m8jBuaP.png)

## Installing MongoDB on Ubuntu V20

We'll first install the most recent licensed version of MongoDB(v4.4) on our Ubuntu Digital Ocean Droplet.

First, login to your droplet and update and upgrade your package `apt` package manager.

- `sudo apt update && sudo apt upgrade`

Next, install MongoDB Community Edition through `apt`

```
# Download the Public GPG key from Mongo Repository. You should get a response 'OK'
wget -qO - https://www.mongodb.org/static/pgp/server-4.4.asc | sudo apt-key add -

# Create a list file for MongoDB on Ubuntu Version 20
echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu focal/mongodb-org/4.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.4.list

# Reload the local package database
sudo apt-get update

# Install the latest version of MongoDB
sudo apt-get install -y mongodb-org

# Pin this MongoDB Version so that it doesn't automatically upgrade packages when a new version becomes available (This is dangerous!)
echo "mongodb-org hold" | sudo dpkg --set-selections && echo "mongodb-org-server hold" | sudo dpkg --set-selections && echo "mongodb-org-shell hold" | sudo dpkg --set-selections && echo "mongodb-org-mongos hold" | sudo dpkg --set-selections && echo "mongodb-org-tools hold" | sudo dpkg --set-selections
```

You can verify that Mongo installed properly by using the `--version` flag as such,
`mongo --version`

## Start and enable MongoDB

Newly installed processes typically need to be `started` in a Linux Operating system like Ubuntu. Starting a process with the `systemctl` command, which is a process responsible for controlling system services on our server, will load all of the Mongo executables into memory and fork the daemon process into the background. The `enable` command is different from `start` and when we `enable` a process, the process will start automatically following a system reboot.

```
# Start MongoDB
sudo systemctl start mongod

# Enable the Mongo Daemon Process
sudo systemctl enable mongod

# Check the status of the Mongo process to make sure it's running
sudo systemctl status mongod
```

You can stop processes from running in the background by issuing the command
`sudo systemctl stop <process>`
To restart a process,
`sudo systemctl restart <process>`

## Enable Access Control (Authentication) with MongoDB

Following the [principle of least privileges](https://us-cert.cisa.gov/bsi/articles/knowledge/principles/least-privilege#:~:text=The%20Principle%20of%20Least%20Privilege%20states%20that%20a%20subject%20should,control%20the%20assignment%20of%20rights.), when configuring an on-disk data store like MongoDB, we shouldn't allow all clients to login to the database as root. Setting up an additional layer of authentication is typically not a bad idea unless it takes away a significant amount of developer time, as breaches to data at rest or in transit can be lethal and potentially cost companies millions.

Unlike MySQL and PostgreSQL, MongoDB **does not** come with a basic level of authentication out of the box. If we don't take extra steps to enable access control and password authentication with MongoDB, any user who accesses the database would have full read/write authorization to all of our data!

Let's setup an `admin` and `user` account on the database.

- The `user` should only be granted permissions to read and write to any collection in a given MongoDB database. They shouldn't be allowed to perform admin tasks such as adding new users and assigning them permissions, creating a new database, or deleting an existing database.
- The `admin` user should be granted full permissions to create and read new databases, users, and collections.

### Start MongoDB without access control enabled

` mongo --port 27017`

### Create an admin user and grant them root privileges.

You can choose any password you want for the admin user, just write it down somewhere or memorize it.

```
--> use admin
--> db.createUser({
    user: "admin",
    pwd: "admin123",
    roles: [ { role: "root", db: "admin" }]
})
```

You can verify your user was added using the `db.getUsers()` command which will list out information about all user in the database. Use the `exit` command to exit the mongo shell.

### Re-start the MongoDB instance with access control enabled

Navigate to the Mongo configuration file where we will enable access control.

`sudo nano /etc/mongod.conf`

The contents of the file should look something like this:

```
# mongod.conf

# for documentation of all options, see:
#   http://docs.mongodb.org/manual/reference/configuration-options/

# Where and how to store data.
storage:
  dbPath: /var/lib/mongodb
  journal:
    enabled: true
#  engine:
#  mmapv1:
#  wiredTiger:

# where to write logging data.
systemLog:
  destination: file
  logAppend: true
  path: /var/log/mongodb/mongod.log

# network interfaces
net:
  port: 27017
  bindIp: 127.0.0.1


# how the process runs
processManagement:
  timeZoneInfo: /usr/share/zoneinfo

#security:
#
...
```

Uncomment the security block and add another like of code so that it looks like this:

```
security:
  authorization: enabled
```

Leave everything else as is. Now, restart MongoDB.

`sudo systemctl restart mongod`

### Connect to the admin database using the admin account

```
mongo --port 27017 -u "admin" -p "admin123" --authenticationDatabase "admin"
```

### Create a new database.

Add a `user` database account who will have `readWrite` permissions to the new database.

For this step, the _exact_ commands below need to be issued in the mongo shell or you will have issues running my senior project application later in this module.

```
--> use brm
--> db.createUser({
    user: "brmUser",
    pwd: "cosc4360-02-fitz-arr",
    roles: [ "readWrite ]
})
```

### Try connecting to the database with a limited access user.

Try connecting to the `brm` database we just created with a limited access user, `brmUser`

`mongo --port 27017 -u "brmUser" -p "cosc4360-02-fitz-arr" --authenticationDatabase "brm"`

If you can successfully login with the `brmUser` using the command above, move on to the next step.

### Learning MongoDB Command Line Syntax

If you're not as familiar with Mongo query syntax, [here's](https://dzone.com/articles/mongodb-commands-cheat-sheet-for-beginners) a nice cheatsheet reference you can bookmark when working on the terminal.

I also advise reading through the official [MongoDB documentation](https://docs.mongodb.com/) when you're researching how to perform various database transactions.

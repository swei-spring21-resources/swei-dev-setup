# MySQL Ubuntu V20.04 Installation and Configuration

MySQL is a popular DBMS (Database Management System) implementing a relational model of persisting data using the SQl query language to manage the data.

In this guide we'll install MySQL Version 8, the most recent stable release. We'll also look at setting up a user account with limited priveleges.

## Insatlling MySQL With apt

The most recent MySQL version available in the Ubuntu package repository is version 8.0.19. As alway, it's a good idea to update your package manager first before installing new software.

- `sudo apt update`

Then, install the `mysql-server` package.

- `sudo apt install mysql-server`

Notice that we don't have to do any special configurations when installing MySQL through `apt` such as entering in passwords. Thus, we need to run some built in security scripts from the DBMS to update the less secure settings included in the default install like sample users.

## Securing MySQL

Run this built-in security script with a `sudo` privileged user:

- `sudo mysql_secure_installation`

Entering this command will take you through a series of security configuration steps.

The first prompt will ask you to setup a validation tool for password plugins. Any new MySQL user will need to create a new password which satisfies our requirements configured here. Choose the `Yes` or entering the `Y` option here to setup your validation metrics for new users.

**Choose either the `1` or `2` option as these are more options.**

```
Securing the MySQL server deployment.

Connecting to MySQL using a blank password.

VALIDATE PASSWORD COMPONENT can be used to test passwords
and improve security. It checks the strength of password
and allows the users to set only those passwords which are
secure enough. Would you like to setup VALIDATE PASSWORD component?

Press y|Y for Yes, any other key for No: Y

There are three levels of password validation policy:

LOW    Length >= 8
MEDIUM Length >= 8, numeric, mixed case, and special characters
STRONG Length >= 8, numeric, mixed case, special characters and dictionary

Please enter 0 = LOW, 1 = MEDIUM and 2 = STRONG: 2
```

Next, you'll setup a password for the `root` user account. Make sure that this password is written down or memorized.

You can go ahead and enter `y` or `Y` fir the following prompts you will receive from this secure installation. This will include responding yes to,

- Removing anonymous users
- Disallowing remote root logins
- Removing test database and access to it
- Reloading Prvilege tables

## Setting up a MYSQL user account with prvileged access

When we install MySQL through `apt` and `mysql-server`, it will create a default `root` user for us, who will be granted full read and write privileges over our MySQL server. Due to the principle of least privileges, we shouldn't use this `root` account unless we need to perform administrative privileges like adding new database user accounts or modifying existing privileges.

**Some operating systems like Ubuntu will enable default authentication for the `root` user using the `auth_socket` plugin method. This means that users like `root` are only authenticated using system credentials rather than a password.** More specifically, as per the [official MySQL documentation](https://dev.mysql.com/doc/mysql-secure-deployment-guide/5.7/en/secure-deployment-configure-authentication.html),

_the `auth_socket` method of user authentication will check whether the socket user name matches the MySQL user name specified between the client program and the server. If the names do not match, the plugin also checks whether the socket user name matches the name specified in the `authentication_string` column of the `mysql.user` table row. If a match is found, the plugin permits the connection._

There are some benefits to using the `auth_socket` method of authentication for our MySQL users. It works very well in instances where we need tightly control access to a MySQL account, such as `root`. However, keep in mind that using an `auth_socket` plugin method of authenticaiton will **disable** remote access to your database for that particular user.

### Create a Database and add a Developer user with limited access

Let's setup a `developer` MySQL account with password authentication and privileged access rights that our teammates can use.

- The developer user will also have a password and only be granted privileges to `SELECT, INSERT, DELETE, UPDATE` data from a specific database on the server. **For your own database, you should grant only the permissions necessary for your teammates. These permissions listed above may be too comprehensive, or not enough depending on your use cases.**

[Here](https://dev.mysql.com/doc/refman/8.0/en/privileges-provided.html) is a reference of the different privileges you can grant user accounts in MySQL with the `GRANT` operator we'll use below.

```
ALL PRIVILEGES- as we saw previously, this would allow a MySQL user full access to a designated database (or if no database is selected, global access across the system)
CREATE- allows them to create new tables or databases
DROP- allows them to them to delete tables or databases
DELETE- allows them to delete rows from tables
INSERT- allows them to insert rows into tables
SELECT- allows them to use the SELECT command to read through databases
UPDATE- allow them to update table rows
GRANT OPTION- allows them to grant or remove other users’ privilege
```

First, login as `root` so that you can add the new user to a database and grant them the correct privileges.

- `sudo mysql`

Next, we'll create a database for our team, and add the `developer` account to that database with the privileges specified above.

```
# Create the database
mysql> CREATE database your_team_database;
mysql> USE mysql;

# Use a password for the developer account, specified after the IDENTIFIED BY operator. You should use a stronger password thanwhat's # entered here in this guide.
mysql> CREATE USER 'developer'@'localhost' IDENTIFIED BY 'dev_password123!';

# Grant specific privileges to the developer user using the database we've preconfigured, your_team_database
mysql> GRANT SELECT, INSERT, DELETE, UPDATE ON your_team_database.* TO 'developer'@'localhost';
mysql> FLUSH PRIVILEGES;
mysql> exit;

```

Try logging in as the limited access `developer` user account in MySQL using the password we setup earlier,

`mysql -u developer -p`

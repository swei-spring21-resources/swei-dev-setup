# Nginx and Letsencrypt SSL Configuration

[DevOps Home](../devops.md)

[Wiki Home](../../README.md)

## Important Online Resources

- [Official Nginx Documentation Serving Static Content](https://docs.nginx.com/nginx/admin-guide/web-server/serving-static-content/)
- [Configuring Nginx as Reverse Proxy](https://docs.nginx.com/nginx/admin-guide/web-server/reverse-proxy/)

[Nginx](https://www.nginx.com/) is a relatively lightweight and powerful webserver for Linux that we'll be using in order to redirect HTTPS requests coming in to our server and to serve static resources (such as HTML pages) back to a client. Nginx has some powerful tools at its disposal, one of the most important (for this guide and project) being the proxy-pass functionality. Nginx acts as a "reverse proxy", listening for normal HTTP/HTTPS requests on ports 80 and 443 (respectively), and then can either reroute the traffic to another webserver (either within the system, that would be listening on a localhost:[port], or to a seperate physical server entirely), serve static web files, invoke PHP or other server-side languages, and a myriad of other functionality not mentioned here.

![Nginx Reverse Proxy Image](https://i.imgur.com/PXhrq5J.png)

First, let's install Nginx. We'll then learn how to use Letsencrypt and Certbot to create our SSL certificate attached to our reserved domain.

## Install Nginx

First, update your package manager. Then install the `nginx-full` flavor for Ubuntu v20. If you're interested in reading about the differences between nginx flavors, here's a [thread](https://askubuntu.com/questions/553937/what-is-the-difference-between-the-core-full-extras-and-light-packages-for-ngi) on this.

- `sudo apt update`
- `sudo apt install nginx-full`

Check your web server version and status to make sure Nginx installed properly.

- `nginx -V`
- `systemctl status nginx`

Make sure you installed Nginx version 1.18 or 1.19.

Next, test out your nginx base install by requesting the default landing page from Nginx. We can easily access the default landing page from Nginx by navigating to our server's IP address.

This command will return your droplet IP address.

- `curl -4 icanhazip.com`

Copy and paste the IP address. Then go to your browser and request the URL:

- `http://<server_ip_here>`

You should see the default landing page of Nginx.

![Nginx default landing page](https://i.imgur.com/NkyhavZ.png)

## Setup a server block using your domain name

You'll be needing to configure your own server blocks for your team, so let's practice writing a block before moving on.

For configuring Nginx, there are two directories that should be called out on the system as important before heading any further:

- `/etc/nginx/sites-available/`
- `/etc/nginx/sites-enabled/`

These two directories serve to hold the Nginx configurations for sites and services that the server should be able to serve back to those who initiate requests to the services. The `sites-available` directory is the hub of all configurations for each service/site/domain that you are going to be hosting on that server, or be proxy-passing to another server using Nginx as a middleman. However, nginx will only use configurations in `sites-enabled` in order to process requests, and we are going to ue this to our advantage. By putting configurations into `sites-available` instead of `sites-enabled`, we can easily take a certain ruleset offline, without having to worry about removing or moving files, since we can make a link from a configuration file in `sites-available` to `sites-enabled`, and that will allow Nginx to find and enable that configuration. This method ensures that there is only 1 version of the configuration on the server at any one time, configurations can be disabled if we so choose, and configurations can exist in `sites-available` that aren't ready to be enabled (such as editing/creating them, and coming back to them).

### Create the root HTML directory

**Note**: For the rest of this guide I will use a domain: `your_domain`. Anywhere you see this domain, replace it with your actual domain name.

```
# Create the html directory where we store our html files. This is where we'll configure Nginx to scan for file names requested by the user.
sudo mkdir -p /var/www/your_domain/html
sudo chown -R $USER:$USER /var/www/your_domain/html

```

We'll also need to assign web root permissions to this directory such that the owner can read, write, and execute files inside this directory, but other users can only read and execute.

- `sudo chmod -R 755 /var/www/your_domain`

Create the HTML file,

- `nano /var/www/your_domain/html/index.html`

```
<html>
    <head>
        <title>Welcome to your_domain!</title>
    </head>
    <body>
        <h1>Success!  The your_domain server block is working!</h1>
    </body>
</html>
```

### Create an Nginx server block for your domain

Now let's setup our custom server block for our domain, and configure an Nginx server block to listen for HTTP requests and respond with our static HTML file we just coded.

`sudo nano /etc/nginx/sites-available/your_domain`

Copy and paste these contents, then save the file. **Remember to replace `your_domain` with your actual domain name.**

```
server {
        listen 80;
        listen [::]:80;

        root /var/www/your_domain/html;
        index index.html index.htm index.nginx-debian.html;

        server_name your_domain www.your_domain;

        location / {
                try_files $uri $uri/ =404;
        }
}
```

**Description**:
I highly recommend reading and bookmarking this [DigitalOcean](https://www.digitalocean.com/community/tutorials/understanding-nginx-server-and-location-block-selection-algorithms) article describing the syntax involved with the Nginx server block above. Another useful source of online documentation is the official [Nginx](https://docs.nginx.com/nginx/admin-guide/web-server/serving-static-content/) documentation. Both of these resources will serve you well throughout the semester if you come across issues setting up Nginx server blocks **(this happens to everyone at some point).**

Briefly summarizing:

- `server`: This is how we define a new server block. There can be multiple server blocks in one Nginx configuration file. For example, we could setup one server block to listen for HTTPS requests, and another to listen to HTTP requests and forward the HTTP to an HTTPS request, which is something we'll do later.
- `listen`: Defines which IP address and port that the server block will respond to. The listen directive can be set to a number of different things, like an IP address/port combo, a lone IP address, or a lone port number as we can see above, which will tell Nginx to listen for HTTP requests on every interface.
- `root`: This is the directory where we tell Nginx to search for static files to respond with.
- `server_name`: This is the our registered domain name. Nginx uses this variable for a myriad of different things you can read about in the documentation linked above.
- `location`: Location blocks are defined in server blocks and tell Nginx how to handle requests for different resources and URI's from the parent `server_name`. There can be multiple locations in one server block. Locations are structured in a hierachial manner, and Nginx will prioritize matching location blocks from top-down when a new client request is handled.

Visual Example:

```
root = "/var/www/your_domain/html"
my domain = "your_domain.com"
User tries to visit: http://your_domain.com/demand/getOrder.html

Nginx will scan the root directory looking for the file = "/demand/getOrder.html"
Example: Successfully found (HTTP 200 Response) --> /var/www/your_domain/html/demand/getOrder.html
Example: Didn't find the getOrder.html file (HTTP 404 Response) --> /var/www/your_domain/html --> demand directory doesn't exist --> Return 404
```

Now, let's link the `sites-available` and `sites-enabled` directory so that our new server block is available, and test our configuration.

```
sudo ln -s /etc/nginx/sites-available/your_domain /etc/nginx/sites-enabled/
sudo nginx -t
sudo systemctl restart nginx
```

Do not move on until the `sudo nginx -t` command responds with a success message on your terminal.

Now go to your browser and visit your domain using an `http` request

- `http://your_domain`

You should see output that the server block is working, or the contents of the HTML file we coded earlier.

## Setup Letsencrypt with Certbot

Finally, we are going to setup HTTPS certificates for our domain. Luckily, Certbot makes it really simple to generate HTTPS certificates and keys with Lets Encrypt. The certs will also be added automatically to our site's Nginx config after they are generated.

Before doing this make sure you've already:

- Configured your Ubuntu droplet with an user account
- Installed Nginx
- Setup a DNS 'A' record for your domain (Hint: The TA's have done this for you, no need to worry.)
- Created and tested an Nginx server block for your domain name. Make sure the command `nginx -t` passes on your terminal.

### Install Certbot and obtain your SSL certificate.

First install the Certbot software on our droplet:

- `sudo apt install certbot python3-certbot-nginx`

Assunming prerequisites have been met, we can not obtain our SSL certificate with the command:

- `sudo certbot --nginx -d your_domain`

The `-d` flap specifies the domain name we'd like to create an SSL certificate for.

Certbot will ask you for your email address as a verification step, and will ask for you to accept the terms and conditions (Yes), and if you'd like to be on the mailing list (you can say no to this), then attempt the challenges to verify the server is setup for https.

You'll then be prompted for an option whether to redirect HTTP traffic to HTTPS. **Pick Option 2 here.**

```
Please choose whether or not to redirect HTTP traffic to HTTPS, removing HTTP access.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
1: No redirect - Make no further changes to the webserver configuration.
2: Redirect - Make all requests redirect to secure HTTPS access. Choose this for
new sites, or if you're confident your site works on HTTPS. You can undo this
change by editing your web server's configuration.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Select the appropriate number [1-2] then [enter] (press 'c' to cancel):
```

If everything was successful, you should see output on your terminal telling your where your new certificates are stored.

## Add the nginx location block for my senior project

The last required step in this tutorital is to add an nginx location block which will `proxy_pass` all requests to our localhost application server currently listening for requests on port 8080.

- `sudo nano /etc/nginx/sites-available/your_domain`

The first server block should look something like this:

```
server {

        root /var/www/your_domain/html;
        index index.html index.htm index.nginx-debian.html;

        server_name your_domain www.your_domain;

        location / {
                try_files $uri $uri/ =404;
        }

    listen [::]:443 ssl ipv6only=on; # managed by Certbot
    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/demand.team11.softwareengineeringi.tk/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/demand.team11.softwareengineeringi.tk/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot

}
```

Replace the code inside the `location /` block with the following:

```
location / {
   proxy_set_header Host $host;
   proxy_set_header X-Real-IP $remote_addr;
   proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
   proxy_pass http://localhost:8080;
}
```

Save this file. Then verify the nginx configuration and restart the nginx systemd service.

- `sudo nginx -t`
- `sudo systemctl restart nginx`

Now, logout of the server. Let's try testing out one of the API's on the application we deployed on our Digital Ocean droplet. Go to your terminal and enter this cURL command.

```
curl -i -X POST -H 'Content-type: application/json' -d '{ "firstName": "Geoff", "lastName": "Arroyo", "siteRole": "SITEADMIN", "email": "geoffarroyo@gmail.com", "username": "geoffarroyo", "phone": "9002920291", "password": "lolpassword" }' https://your_domain_here/createSiteAdmin
```

**Make sure to replace with your_domain_here wth your actual domain name.** Then run it. You should get back a HTTP 200 response from the server.

_If you're using Windows, you likely won't have the cURL program already installed. Refer to [this video tutorial](https://www.youtube.com/watch?v=8f9DfgRGOBo&ab_channel=AutomationStepbyStep-RaghavPal) which will walk you through how to install setup the cURL program on your system._

_After creating the user above_, you can also verify that your droplet was properly configured by going to your browser and entering the URL address:

`https://your_domain/getUsersBySiteRole?siteRole=SITEADMIN`

You should see a JSON response on your browser with the user you just created.

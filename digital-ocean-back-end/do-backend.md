# (Optional) Setting up a more sophisticated Digital Ocean back-end programming environment

[Back to Wiki Home](../README.md)
_Written by: Geoff Arroyo_

_03/22/21_

If you have some extra time, try setting up your Digital Ocean environment such that back-end developers can easily test their services without running into authentication or authorization issues. This will take a bit of time, but will end up saving your team a ton of time down the road when the need for manual testing and debugging becomes more apparent.

## Setting up Bitbucket SSH Git Clone on the droplet

There are multiple ways to clone a repository. The first is though HTTPS, which is selected by default on Bitbucket when you clone a repository. We can also use the SSH protocol to clone the repository onto our computer. The SSH method is generally more secure while also being more convenient in the long-run, as we won't have to enter our bitbucket password everytime we need to pull down remote assets or push code to the remote Bitbucket repository. First we'll clone the repositories onto the Digital Ocean droplet using an SSH key that we setup with our Bitbucket workspace.

### Get Administrator Access to your team's Bitbucket workspace

**Any teammate who wants to manually test code on the droplet should do this step.**

First, contact one of the TA's via Slack or discord so that we can grant you Bitbucket admin access to your team's workspace. After we send you an email invite to update your permissions to the workspace, you should now be able to go to your Bitbucket console and click on your team's workspace link (e.g. `SWE Spring 2021 Team 13`).

If you can't find your team's workspace link After you click on your team's workspace link, you should be see a `Settings` wheel on your left sidebar. If you can't see the Settings wheel, you don't have admin access to your team's workspace and won't be able to move forward until this is resolved.

![Bitbucket workspace preview](https://i.imgur.com/JZEFevB.png)

### Create the SSH key pair on the Digital ocean droplet to your team's bitbucket workspace.

**Any teammate who wants to manually test code on the droplet should do this step.**

Now, login to your DigitalOcean droplet. Create a new SSH keypair using the command

- `ssh-keygen -t rsa -b 4096`

Skip the passphrase by hitting enter on the terminal twice. When the SSH agent asks you where you want to store the new private key, I suggest **not using the default id_rsa** option, as we can be more descriptive with our identity file name. Assuming that you have a home user directory setup on the DigitalOcean droplet (e.g. `/home/geoff`) I would recommend naming your identity file,

- `/home/your_username/.ssh/bitbucket_rsa`

Go ahead and replace `your_username` with your actual linux user. You should get a confirmation that your key pair was successfully created.

Now, we need to copy the **public key** you just created with your `ssh-keygen` commmand onto the Bitbucket workspace settings, such that we as a Bitbucket user are authenticated via SSH to access any repositories from the server.

- `cd ~/.ssh`
- `cat bitbucket_rsa.pub`

Copy the contents of the `.pub` file you see on your terminal.

### Paste the public key contents into the Bitbucket workspace.

**Any teammate who wants to manually test code on the droplet should do this step.**

After copying your public key file you created on the server, head back to your Bitbucket console.

- Find your team workspace homepage. You can do this by clicking on your Bitbucket icon on the bottom left of your console, then clicking into your workspace name, or `Show all workspaces`.
- Navigate to `Settings -> SSH Keys`
- Click on the `Add key` button.
- Go ahead and give your new key a title, such as `Geoff Demand Server Public Key`
- Paste the contents of the public key file you created on the server. Then click on `Add key`.

![Bitbucket workspace settings image](https://i.imgur.com/NiZOcOs.png)

![Bitbucket workspace SSH image](https://i.imgur.com/P710Cqf.png)

You should see your new key show up on the Bitbucket console.

### Add ssh authentication to your DigitalOcean Linux user profile

**Any teammate who wants to manually test code on the droplet should do this step.**

There are certain scripts we can run when we as a Linux user login to the server. One of these commands we want to run has to do with an `ssh-agent`, or a program in Linux distributions that can store our identity files such that when we use the SSH protocol to access Bitbucket repsitories, our host (DigitalOcean) will know which key to use for authentication.

Log in to your team's DigitalOcean droplet. We'll add a few commands that we want to execute so we're automatically authenticated to the Bitbucket client every time we login.

- `cd ~`
- `sudo nano .profile`

Go ahead and enter these contents **at the bottom of the .profile file you just opened, but make sure to replace the path I have below to an identity file (your private key you created earlier, `bitbucket_rsa`) with your own path**.

```
eval `ssh-agent`
ssh-add /home/geoff/.ssh/bitbucket_rsa
```

**Important**
_If you're using a Python virtual environment, add another line to the bottom of your `.profile` script which will activate the Python virtual environment immediately after you login. Replace the path given in the command below with the path to your specific virtual environment._

`source /home/geoff/environments/team13environment/bin/activate`

Save and exit the file. Now, refresh the profile script we just edited to test out whether or path was properly set.

- `source .profile`

If everything went smoothly, your private identity file key should give you push/pull access to your team's repositories on the server.

## DevOps - Cloning Bitbucket Repositories onto the droplet, setting up linux group for back-end developers

**Only one member needs to complete this step - likely DevOps**

Assuming that we have our identity file (`bitbucket_rsa`) loaded into the `ssh-agent`on the droplet, we can now clone the repositories onto the server using the SSH protocol. However, let's first setup a Linux group for back-end developers. By creating this group, we'll give the group read/write/execute access to the code inside of the repositories on the server, and not just our user.

### Setting up a back-end Linux group

```
sudo groupadd back_end

# Add all of your teammates accounts to this group who need access to manually test on the server, including yourself.
sudo adduser geoff back_end
sudo adduser ruben back_end
```

You can use the `groups <username>` command to see which groups a user belongs to.

### Clone the repositories onto the droplet using the SSH URL generated by Bitbucket.

Go to first repository you want to clone on your Bitbucket console. You should see a `clone` button on your repository homepage. Click on this.

On the top right of the popup window, you should see a dropdown with the `HTTPS` protocol selected by default. Change the clone option to `SSH`, then copy the URL it gives you.

![Bitbucket SSH clone preview](https://i.imgur.com/8NeGaV5.png)

Go back to the terminal and create a directory where you will clone all of your team's repositories. For this tutorial, I will use `/home/team13/repos` as an example.

- `cd /home/team13/repos`
- `git clone ssh_URL`

Now, we should recursively set the folder permissions and ownership such that all of the users in our Linux user group, `back_end`, we added earlier, all can read/write/execute from the files inside of the repositories off of the directory `/home/team13/repos`.

- `cd /home/team13`
- `sudo chown -R $USER:back_end repos`
- `sudo chmod -R 775 repos`

Go ahead and clone the rest of your repositories using the SSH clone method.

### Make sure that the linux group back_end has access to your Python virtual environment, if you've set one up.

**If you have a Python virtual environment setup on your server**, something else you'll also want to check is to make sure that the `back_end` linux group has write access to the virtual environment in case they ever need to manually install another Python package on the server.

Remember to replace the paths used below with your own paths to your own virtual environment folder.

- `cd ~/environments`
- `sudo chown -R $USER:back_end team13environment`
- `sudo chmod -R 775 team13environment`

## Setting up a requirements.txt file in your back-end Python repositories

One of the common problems you've probably already experienced with large software projects is managing software packages and dependencies when we deploy code to new environments, such as deploying our Python services from our computer to the DigitalOcean droplet.

One of the ways we can manage Python dependencies is using a `requirements.txt` file generated by the `pip` Python package manager. Inside of the `requirements.txt` file which we will add to our repository, will be a list of Python packages and version numbers which are required by the project to run.

The `requirements.txt` file will also help others who want to run our project easily install all of the packages our project requires.

First, navigate to your python project (use your computer, not the server).

- `pip freeze > requirements.txt`

This command will create a new `requirements.txt` file in your repository, as described above. Now you and your teammates can easily install of the packages via `pip` using the command,

- `pip install -r requirements.txt -v`

# Markdown Syntax Guide
You can use this guide to write more documentation that we can use
> NOTE: This document was transcribed from bitbucket's guide on markdown

---

## Markdown Syntax
The page below contains examples of Markdown syntax.

### Headings
```
  # This is an H1
  ## This is an H2
  ...
  ###### This is an H6

  This is also an H1
  ==================

  This is also an H2
  ------------------
```

### Paragraphs
paragraphs are separated by empty lines, to create a new paragraph press `return` twice
```
    Paragraph 1

    Paragraph 2
```

### Character Styles
```
    *Italics*
    _Italics_
    **Bold**
    __Bold__
    ~~Strikethrough Text~~
```

### Unordered Lists
```
    * Item 1
    * Item 2
    * Item 3
        * Item 3a
        * Item 3b
        * Item 3c
```

### Ordered Lists
```
    1. Step 1
    2. Step 2
    3. Step 3
        1. Step 3.1
        2. Step 3.2
        3. Step 3.3
```
> NOTE: These can be used in combination

### Quotes or Citations
```
    Here is a quote:
    > Neque porro quisquam est qui
    > dolorem ipsum quia dolor sit amet,
    > consectetur, adipisci velit...
```

### Inline code
```
    Use the backtick (`) to refer to a single line of code
    `math.pow(2,2)`
```

### Code Blocks
```
    The code block needs to be indented within its markers

    This is a sample paragraph

        This is a codeblock
        the code can span multiple lines

    alternatively, you can add three backticks together around your code to make another code block

    ```
        This is a codeblock
    ```

    Syntax highlighting is available and looks like this
    ```py
        if loginFailed():
            kickUserOut()
    ```
    There are many different languages that are supported,
    google: "markdown syntax highlighting" to see which keyword needs to be used,
    often it's the name of the language
```

> This document is currently incomplete and will be added to over the course of this semester

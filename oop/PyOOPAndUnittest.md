# Python Object-Oriented Programming and Unittest Module

[Back to Home](../README.md)

This module should be completed by **two** team members as we'll look at implementing object-oriented classes in Python, writing unit tests to ensure the quality of our python class, and learning the fundamentals of version control through Git as well. Having two members is important for this module as you'll start learning basic Git commands which are used for working on collabarative software projects with your teammates. If you have an **odd** number of teammates (5) on your team, the 'odd' person out should still practice writing the unit tests and object-oriented class described in this module.

Here is the [link](../vcs/vcs.md) to the version control documentation which will be referenced throughout this module. You'll need to use the documentation provided in the version control module as a reference to complete this module with your teammate. The Version-control resources and documentation will be available to everyone throughout the semester and updated as needed.

Splitting up the work:
**Both teammates**

- Re-learn and implement Python OOP Classes and Unit testing
- Learn and apply basic Git knowledge
- Become familiar with Bitbucket UI through pull requests, repository settings, and creating branches

**Teammate 1 Tasks**

- Setting up a new bitbucket repository on their student account
- Adding a TA and teammate to their repository
- Writing unit tests for your teammates Python class

**Teammate 2 Tasks**

- Create a new git branch in teammate 1's repository
- Write a Python Car Class
- Create a new Pull Request on Bitbucket and add one TA as required reviewer

## Step 1: Teammate 1 creates private Bitbucket repository

Teammate 1 should first create a repository using their Bitbucket student account. Follow the [documentation](../vcs/vcs.md#markdown-header-create-a-repository-on-your-bitbucket-account) provided in the version control module for a guide on how this should be setup. You should finish the **creating repository** and **adding new users to your repository** sections.

## Step 2: Both teammates clone the repository and setup their Git Config

Both teammate 1 and 2 should now clone the repository onto their local system. The [documentation](../vcs/vcs.md#markdown-header-clone-the-repository-onto-your-system-and-setup-git-configuration) for doing this is provided in the version control here.

## Step 3: Teammate 2 creates a new branch and writes the Object-Oriented Python Car Class

Now, teammate 2 should have access to the repository setup by teammate 1.

Teammate 2 should create a new version control branch on their system, titled `CarImpl`. The version control [documentation](../vcs/vcs.md#markdown-header-create-a-new-development-branch) for git branching basics will be useful if you're not already familiar with how to do this. (See **Create a new development branch section**)

In this course, we'll use Python OOP to model real-world entities in our transportation as a service application. As a review of Object-oriented programming in Python, we'll write a simple `Car.py` class in the `CarImpl` branch. The `Car` class defined in this source file should have the following attributes:

- make
- model
- year
- license_plate
- speed = 0
- odometer = 0
- trip_time = 0

Use a Python constructor in your `Car` class which will 4 attributes above as parameters:
`def __init__(self, make, model, year, license_plate)`

Set the other instance attributes to their default values (of 0). Use _private_ instance variables in your class.

Next, Define **getters and setters** in your `Car` class. There's a few different ways to do this in Python.

One common way is to use getters and setters as we would in Java:

```
# Class constructor
def __init__(self, a):
    self.__a = a

# Getter method yo get the properties using the self object
def get_a(self):
    return self.__a

# Setter method to change the value of attribute 'a' using the self object
def set_a(self, a):

    if a > 0 and a % 2 == 0:
        self.__a = a
    else:
        self.__a = 2
```

Another way to write getters and setters in Python is using the `@property` and `@setter` notation:

```
class Foo:
    def __init__(self, var):
        # Initialize the instance attributes
        self.__a = var

    # Use the property annotation to get the value of a private attribute without any getter methods.
    ## We could retrieve the property as such,
    ## foo = Foo(12)
    ## print(foo.a) --> 12
    @property
    def a(self):
        return self.__a

    # The attribute name and the method name MUST be the same which is used to set the value for the attribute.
    @a.setter
    def a(self, var):
        if var > 0 and var % 2 == 0:
            self.__a = var
        else:
            self.__a = 2
```

Add some basic input validation to the setters of your class methods such as:

- Making sure the speed, odometer, and trip_time values of the car aren't set to a negative value

Write a `__str__(self)` or "toString()" method in your class which will print out all of the instance's current attributes in a format such as:

```
Car
Make: Toyota
Model: ...
........
```

You should also define custom class methods which will be used by your partner to unit test,

- `def accelerate(self) --> Increment the speed of the car by 5.`
- `def decelerate(self) --> Decrement the speed of the car by 5. If the speed of the car is already less than 5, set the speed of the car to 0.`
- `def brake(self) --> Set the speed of the car to 0.`
- `def stepRoute(self) --> Increment the odometer of the car by the speed value of the car. Increment the trip_time of the car by 1.`
- `def averageSpeed(self) --> Check the average speed of the car instance by dividing the odometer of the car by the trip time. If the trip time is equal to 0, return 0.`

## Step 4: Teammate 2 add, commit, and push your code to your remote repository

Next, teammate 2 will add, commit, and push their `Car` class to the remote repository. Make sure to check the VCS [documentation](../vcs/vcs.md#mardown-header-stage-your-new-commits,-commit-them,-and-push-them;-seting-up-a-remote-tracking-branch) for an explanation of how to do this from the command line. It's important to remember that after every time we finish implementing and testing our code on our IDE, we can share it with our teammates using the `add, commit, and push` version control workflow in Git.

After you push your new `Car` class to your repository, login to your Bitbucket account and go to the remote repository where the source code is. You'll need to switch branches using the UI before you can see your `Car` class as we created a new remote branch during this step and the previous step.

## Step 5: Teammate 1 Pull in Changes from teammate 2

Now teammate 1 should pull in the additions made through the implementation of `Car.py` from teammate 2, as you (teammate 1) will need to write unit tests for your teammates python class and execute them on your local system.

Here's the VCS module [documentation](../vcs/vcs.md#markdown-header-fetch-the-new-development-branch-and-pull-in-the-changes-that-were-made) for research links on how to use the `git pull` and `git checkout` commands.

## Step 6: Teammate 1 Writes Unit tests for the Car Python class

Unit tests are a type of [software test](https://www.atlassian.com/continuous-delivery/software-testing/types-of-software-testing) which strictly tests the source code of our application. In a unit test, our code is sandboxed from any outside resources or integration schemes so that we can solely focus on testing the correct behavior and logic of the code. In software production environments, unit tests are usually automated and will test the functions and methods of different components or classes of software.

In this course we require Python unit tests to ensure the quality of our code that we'll push to our DigitalOcean cloud production environment. We'll use the `unittest` framework in the Python 3 library to write our unit tests.

Here is a sample of how to setup your first unit test.

```
import unittest

class TestCarClass(unittest.TestCase):
    def testCarConstructor(self):
        car = Car('sedan', .....)
        expected = 'sedan'
        self.assertEqual(expected, car.model)
        # other tests go here to test the constructor

    # Write your other unit tests here

if __name__ == '__main__':
    unittest.main()
```

Read over our own additional Python unittest [documentation](../unittesting/unitTesting.md) to learn more about more unit testing methods, setting up and tearing down testing code, and unit test organization. You should study the official python unittest [documentation](https://docs.python.org/3/library/unittest.html) as well.

Pull up your IDE and make sure that you're in the right `CarImpl` branch by using the command `git status`. You should already have access to your teammate's `Car.py` Python class at this point.

Add a new test file to the `CarImpl` branch titled `CarTest.py`

- First, write a `setUp()` method in your unit test which will construct a default (and valid) Car instance. Code inside the `setUp()` method is executed before every individual unit test in the test file.

Write unit tests which will verify that:

- A Car instance can be constructed properly
  - Check the value of each attribute set in the constructor using the same `Car` instance written in the `setUp()` method.
- The Car class setters work as expected
  - Check each each setter method in the `Car` class. For example, change the `Car` speed to 5 and make sure that the instance attribute value was updated accordingly.
- Test the `accelerate` method and make sure that it's working as expected.
- Test the `decelerate` method and make sure that it's working as expected.
- Test the `brake` method and make sure that it's working as expected.
- Test the `step_route` method and make sure that the car instance `trip_time` and `odometer` are being updated as expected.
- Test the `average_route_speed` method. Give the car instance a few route steps and then check to make sure the return value is as expected.
  - Also, write another unit test for `average_route_speed` where you try to caluclate the average route speed before the car has travelled any distance on a route. In this case, you should expect that to receive 0 as the return value.

**Important**
_Eventually, you'll need to unit test that an error was thrown in your code if incorrect data was passed into a class constructor or method. You can find an example of how to test that an Error was successfully raised using the `unittest` [documentation](https://docs.python.org/3/library/unittest.html)_

_After_ all of the unit tests you've written have passed, add, commit, and push your updates to the remote `CarImpl` branch.

# Step 7: Teammate 2 Submit new Pull Request on Bitbucket

After teammate 1 has finished writing and executing their unit tests and pushed them to the remote `CarImpl` branch, teammate 2 should create a new pull request on Bitbucket. [Here's](../vcs/vcs.md#markdown-header-submit-a-new-pull-request-on-bitbucket-with-one-ta-as-a-required-reviewer) the VCS module documentation for instructions on creating a pull request.

Teammate 1 and 2 should follow the VCS documentation to complete the remainder of this module.

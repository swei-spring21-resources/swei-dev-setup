# REST Bootcamp Module

[Back to Home](../README.md)

In this module, we'll do a short exercise to introduce you to REST. **REST** stands for Representational state transfer and is a microservice style which defines a set of rules which are used when developing webservices. REST is a very common type of API used in the software industry. There are several constraints which define RESTful webservices from non-RESTful webservices, one of the most important being that a REST webservice should be _stateless_ such that each client request made to a server should contain all of the necessary metadata for a response. In REST, their should be a uniform response from the server given a uniform request, and the server can't store any data or contexts when generating a response.

An **API** stands for Application Programming Interface and is an software interface which determines resource transactions between two endpoints. API's govern how software clients should format their request, what types of data should be used, which HTTP method to use, and so on. API's also define how hosts or recipients of client requests should respond back.

If you want to deep dive into more REST and API learning resources, here a few recommendations:

- https://www.youtube.com/watch?v=Q-BpqyOT3a8&ab_channel=TraversyMedia
- https://restfulapi.net/

We'll be coding a simple application server which will utilize REST API's already written by [Openweathermap](https://openweathermap.org/api). Our application server will respond to client requests (from localhost) to

- get current weather data in Austin
- get the current temperature in any city that the user wants
- get a daily forecast with a timestamp and temperature included in the JSON response.

In the second part of this module you'll take a look at using [Postman](https://www.postman.com/), which is an easy-to-use application to test your REST API endpoints.

## Register a free account with the OpenWeatherMapAPI

First you'll need to [register a free account](https://openweathermap.org/register) with the OpenWeatherMap API so that you're authenticated to use their API's. After creating your account you should get an email confirmation back from OpenWeatherMap along with your API key. An API key is how you're personally authenticated to utilize software resources and shouldn't be shared with anyone.

Copy the API Key as you'll need this to format the HTTPS requests you make to the OpenWeatherMap API which we'll be writing in Python.

## Setup a base application server using localhost and Python BaseHTTPRequestHandler

Since we're writing our own REST API, we'll need to first setup an applcation server which will _listen_ for client requests on a specified port using a specified protocol and HTTP method.

We'll use the Python` BaseHTTPRequestHandler` package from the Python `http.server` module. You'll want to use this [link](https://docs.python.org/3/library/http.server.html) as a reference when learning how to format your application server.

For now we'll just write a base configuration which will respond to a GET request from a client with a string 'Hello World' on the browser to make sure there are no connection issues.

Here's some code to get you started....

```
from http.server import BaseHTTPRequestHandler, HTTPServer
import requests
import json

class WeatherServiceHandler(BaseHTTPRequestHandler):
    # Respond to GET requests from the client
    def do_GET(self):
        path = self.path

        # This is the URI or block of code which will execute when the client requests the address: 'http://localhost:8082'
        if path == '/':
            self.send_response(200)
            self.send_header("Content-type", "text/html")
            self.end_headers()

            encodedString = "Hello World".encode('utf-8')
            # Write the encoded string (the write function in this package only accepts bytes data) back to the client's browser
            self.wfile.write(encodedString)

        # Define other GET Endpoints here (e.g., elif path == '/getCurrentHumidity')

# Turn the app server on at port 8082 and fork it
if __name__ == "__main__":
    serverPort = 8082
    hostName = "localhost"
    appServer = HTTPServer((hostName, serverPort), WeatherServiceHandler)
    print("Server started http://%s:%s $ (hostName, serverPort))

    try:
        appServer.serve_forever()
    except KeyboardInterrupt:
        pass

    appServer.server_close()
    print("Server stopped")
```

Ideally this code should be in a service file titled something like `WeatherServiceHandler.py`. Execute the file and you should see a print confirmation message letting you know that the app server is running on your system. Go to your browser and type in: `http://localhost:8082/`. You should see a Hello World String on your browser.

## First REST Endpoint: Get weather data for the city of Austin

Now we'll write a custom endpoint which will get weather data for the city of Austin. This weather data will include the current temperature in Austin, humidity, wind speeds, and so on.

Here's the [OpenWeatherMap API documentation](https://openweathermap.org/current) for the endpoint which we'll use in our python weather service.

First, copy your API key into your `WeatherServiceHandler.py` file. Here is a sample of how you would utilize the API:

```
import requests
import json

api_key = "13rg24g24tgttt2223442frg45g"
default_city = "Austin"
weatherUrl = "https://api.openweathermap.org/data/2.5/weather?q=%s&appid=%s&units=standard" % (default_city, api_key)

response = requests.get(weatherUrl)
weatherData = json.dumps(response.text)

print(weatherData)
```

Now you'll need to define an endpoint in your service that will write the JSON weather data returned from the OpenWeatherMap API back to the user's browser. The [URI](https://restfulapi.net/resource-naming/#:~:text=REST%20APIs%20use%20Uniform%20Resource,intuitive%20and%20easy%20to%20use.) (Uniform resource identifier) for this API will be root '/'. This API should only respond to HTTP GET requests from the client (you can reuse code from the previous step)

**Expected output:**

- When you go to your browser and type a GET request at: `http://localhost:8082/`, you should get back weather data as a JSON response on your browser which should look something like this:

```
{"coord": {"lon": -97.74, "lat": 30.27}, "weather": [{"id": 803, "main": "Clouds", "description": "broken clouds", "icon": "04d"}], "base": "stations", "main": {"temp": 283.48, "feels_like": 277.79, "temp_min": 283.15, "temp_max": 284.26, "pressure": 1018, "humidity": 53}, "visibility": 10000, "wind": {"speed": 5.54, "deg": 321}, "clouds": {"all": 75}, "dt": 1609532140, "sys": {"type": 1, "id": 3344, "country": "US", "sunrise": 1609507650, "sunset": 1609544494}, "timezone": -21600, "id": 4671654, "name": "Austin", "cod": 200}
```

## Second REST endpoint: Get the current temperature at any city location

Next we'll write an endpoint in our REST API which will return the current temperature (in standard units) of any city which the user requests.

Notice that from the previous API endpoint we wrote, we already have access to temperature data. So we can re-use the same the OpenWeatherMap API in our service, except this time we'll allow the user to pass a 'city' paramater into their HTTP GET request.

Define a new URI for this API titled `/getCurrentTemperature` which will respond to GET requests.

Expected output:

- When you go to your browser and type a GET request at: `http://localhost:8082/getCurrentTemperature?city=Boston`, you should get back the temperature on your browser of the city which was passed into the request,
  `275.65`

_Hint:_
We now have parameters we need to strip from the client URL (the city name of the request). Here are some references which should help you achieve this:

- https://docs.python.org/3/library/urllib.parse.html
- (look @ response with the most upvotes here): https://stackoverflow.com/questions/3788897/how-to-parse-basehttprequesthandler-path

_Hint:_
When you're writing URI's for API's requiring a GET method, you can utilize the `in` operator in python so that you string match the prefix of the URL, similar to a regular expression match:
`elif '/getCurrentTemperature' in path`

## Third REST endpoint: Get the hourly forecast of any city

The last API endpoint we'll write will return the hourly forecast of the city which the user requests. The client will pass in a city as a string for us.

More specifically, your API should respond to forecast requests by returning back a 2-d array of data, where each nested array contains a timestamp and a temperature of the given city location.
e.g.,
`hourlyForecast: [ [ ""2021-01-06 15:00:00-06:00", 276.27 ], ["2021-01-06 18:00:00-06:00", 274.82 ], ... ]`

This API will be respond to a HTTP POST request from the client rather than a GET request. We'll test out this final forecast API by using Postman.

Here is the [OpenWeatherMap API documentation](https://openweathermap.org/forecast5) for the forecast API we'll be using in our service.

`URL = "https://api.openweathermap.org/data/2.5/forecast?q=%s&appid=%s&units=standard" % (cityParam, api_key)`

Here is a sample response from this API:

```
{
  "cod": "200",
  "message": 0.0122,
  "cnt": 40,
  "list": [
    {
      "dt": 1519074000,
      "main": {
        "temp": 283.99,
        "temp_min": 281.801,
        "temp_max": 283.99,
        "pressure": 989.94,
        "sea_level": 1029.29,
        "grnd_level": 989.94,
        "humidity": 52,
        "temp_kf": 2.19
      },
      "weather": [
        {
          "id": 801,
          "main": "Clouds",
          "description": "few clouds",
          "icon": "02d"
        }
      ],
      "clouds": {
        "all": 20
      },
      "wind": {
        "speed": 3.36,
        "deg": 325.001
      },
      "rain": {},
      "sys": {
        "pod": "d"
      },
      "dt_txt": "2018-02-19 21:00:00"
    },
    {
      "dt": 1519084800,
      "main": {
        "temp": 282.64,
        "temp_min": 281.177,
        "temp_max": 282.64,
        "pressure": 990.6,
        "sea_level": 1029.94,
        "grnd_level": 990.6,
        "humidity": 47,
        "temp_kf": 1.46
      },
      "weather": [
        {
          "id": 802,
          "main": "Clouds",
          "description": "scattered clouds",
          "icon": "03n"
        }
      ],
      "clouds": {
        "all": 36
      },
      "wind": {
        "speed": 3.17,
        "deg": 319.502
      },
      "rain": {},
      "sys": {
        "pod": "n"
      },
      "dt_txt": "2018-02-20 00:00:00"
    },
    ...
  ],
  ...
}

```

Notice that the response we'll get back from OpenWeatherMap contains a `list` attribute. We can utilize a for loop to go through the list of weather objects and extract the data we need,

```
hourly = data["hourly"]

for entry in hourly:
    dt = datetime.fromtimestamp(entry["dt"], pytz.timezone("US/Central"))
    temp = entry["main"]["temp]
```

Define a new URI for this API titled `/getHourlyForecast` in your python weather service, which will respond to POST requests.

_Hint_:
Since this API will respond to HTTP POST requests, you'll need to add a `do_POST(self)` function to your service, similar to how we've already defined a `do_GET(self)` function.

_Hint_:
A POST request has usually has a body of data, which will be entered by the client when they make the request. In this case, we should expect one body parameter, a `city`. Here's some code you can use in your `do_POST(self)` function which will extract the POST body values into a python dictionary for you to use:

```
postBodyLength = int(self.headers['content-length'])
postBodyString = self.rfile.read(postBodyLength)
postBody = json.loads(postBodyString)

print(postBody['city'])
```

_Hint_:
If you use the datetime code above, and try to return your 2-d array as JSON data using `json.dumps()`, you'll likely encounter an error `datetime.datetime not JSON serializable`. Check out this [thread](https://stackoverflow.com/questions/11875770/how-to-overcome-datetime-datetime-not-json-serializable/36142844#36142844) for a solution.

## Test out your forecast API on Postman:

First, download the [Postman Desktop App](https://www.postman.com/downloads/). You can also use the browser version of Postman but I've ran into previous issues with the browser.

You'll need to setup an account with Postman. You can create an account for free. Also, make sure that you're app server is still running on port 8082 on localhost, otherwise this won't work.

After creating your account you'll see different options. Press on the orange 'New' button on the left of the app window. Then click on create new request.

![Postman welcome snapshot](https://i.imgur.com/iieqBg5.png)

Postman will prompt you to enter some details about the request so you can re-use it for later.

- Request name: HourlyForecastIntegTest
- Description: Testing my hourly forecast API for my bootcamp module.
- Create a new collection. You can name it anything you want to.

![Postman request setup snapshot](https://i.imgur.com/A2XIXg6.png)

Next you'll see the Postman request page.

- Go to the url bar and make sure the HTTP method is set to POST.
- Type in the URL of your API: `localhost:8082/getHourlyForecast`
- Enter the city key value pair by clicking on the `Body` option underneath the URL bar. Then click on the `raw` select option. Make sure that the JSON option is applied to your raw data, you can validate this as there will be a dropdown with different data encoding types underneath the URL bar.

![Postman data snapshot](https://i.imgur.com/JUiqNKV.png)

Click on the blue send button. You should see a JSON response similar to this:

```
{
    "hourlyForecast": [
        [
            "2021-01-01 21:00:00-06:00",
            274.68
        ],
        [
            "2021-01-02 00:00:00-06:00",
            275.17
        ],
        [
            "2021-01-02 03:00:00-06:00",
            277.41
        ],
        [
            "2021-01-02 06:00:00-06:00",
            279.72
        ],
        [
            "2021-01-02 09:00:00-06:00",
            282
        ],
        [
            "2021-01-02 12:00:00-06:00",
            284.93
        ],
        [
            "2021-01-02 15:00:00-06:00",
            282.66
        ],
        [
            "2021-01-02 18:00:00-06:00",
            277.77
        ],
        [
            "2021-01-02 21:00:00-06:00",
            275.46
        ],
        [
            "2021-01-03 00:00:00-06:00",
            274.61
        ],
        [
            "2021-01-03 03:00:00-06:00",
            273.95
        ],
        [
            "2021-01-03 06:00:00-06:00",
            273.69
        ],
        [
            "2021-01-03 09:00:00-06:00",
            276.23
        ],
        [
            "2021-01-03 12:00:00-06:00",
            277.41
        ],
        [
            "2021-01-03 15:00:00-06:00",
            276.77
        ],
        [
            "2021-01-03 18:00:00-06:00",
            276.65
        ],
        [
            "2021-01-03 21:00:00-06:00",
            276.35
        ],
        [
            "2021-01-04 00:00:00-06:00",
            277.02
        ],
        [
            "2021-01-04 03:00:00-06:00",
            277.61
        ],
        [
            "2021-01-04 06:00:00-06:00",
            278.13
        ],
        [
            "2021-01-04 09:00:00-06:00",
            278.13
        ],
        [
            "2021-01-04 12:00:00-06:00",
            276.71
        ],
        [
            "2021-01-04 15:00:00-06:00",
            275.23
        ],
        [
            "2021-01-04 18:00:00-06:00",
            274.47
        ],
        [
            "2021-01-04 21:00:00-06:00",
            274.35
        ],
        [
            "2021-01-05 00:00:00-06:00",
            274.51
        ],
        [
            "2021-01-05 03:00:00-06:00",
            275.15
        ],
        [
            "2021-01-05 06:00:00-06:00",
            274.69
        ],
        [
            "2021-01-05 09:00:00-06:00",
            275.51
        ],
        [
            "2021-01-05 12:00:00-06:00",
            276.98
        ],
        [
            "2021-01-05 15:00:00-06:00",
            276.07
        ],
        [
            "2021-01-05 18:00:00-06:00",
            275.48
        ],
        [
            "2021-01-05 21:00:00-06:00",
            274.35
        ],
        [
            "2021-01-06 00:00:00-06:00",
            273.55
        ],
        [
            "2021-01-06 03:00:00-06:00",
            273.48
        ],
        [
            "2021-01-06 06:00:00-06:00",
            273.52
        ],
        [
            "2021-01-06 09:00:00-06:00",
            275.46
        ],
        [
            "2021-01-06 12:00:00-06:00",
            276.82
        ],
        [
            "2021-01-06 15:00:00-06:00",
            276.27
        ],
        [
            "2021-01-06 18:00:00-06:00",
            274.82
        ]
    ]
}
```

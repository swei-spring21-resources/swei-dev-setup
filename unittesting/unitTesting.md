# Python Unit Testing

By: Ruben Ruiz

---
## Purpose
Unit testing allows us to better automate and maintain our tests over time

---
## Getting started
To get started, you must code a test module.

### Unit Test Sample File
```py
    test_<filename>.py
    
    import unittest
    import <filename>
    
    class Test<FileName>(unittest.TestCase):
        
        def test_<testName>(self):
            #Add test cases here, reference the assertions below
```

### Types of assertions
|MethodName|Its Equivalent in Python3|How to use it|
|----|----|----|
|assertEqual(a,b)|a == b|`self.assertEqual(a,b)`|
|assertNotEqual(a, b)| a != b| `self.assertNotEqual(a,b)`|
|assertTrue(x)| bool(x) is True| `self.assertTrue(x)`|
|assertFalse(x)| bool(x) is False| `self.assertFalse(x)`|
|assertIs(a, b)| a is b| `self.assertIs(a,b)`|
|assertIsNot(a, b)| a is not b| `self.assertIsNot(a,b)`|
|assertIsNone(x)| x is None| `self.assertIsNone(x)`|
|assertIsNotNone(x)| x is not None| `self.assertIsNotNone(x)`
|assertIn(a, b)| a in b| `self.assertIn(a,b)`|
|assertNotIn(a, b)| a not in b| `self.assertNotIn(a,b)`|
|assertIsInstance(a, b)| isinstance(a,b)| `self.assertIsInstance(a,b)`|
|assertNotIsInstance(a, b)| not instance(a,b)| `self.assertNotIsInstance(a,b)`|

> There are some more assertions surrounding testing exceptions, but good practice would be to handle exceptrions in the file itself

|MethodName|Its Equivalent in Python3|How to use it|
|----|----|----|
|assertRaises(<exception>,<function>,[values])| ...| `self.assertRaises(<Error>, <Function>, a, b)`|
|et. al.|---|---|
### How to run tests
Do either
```console
$ python -m unittest test_<filename>.py 
```
Or include this at the bottom of the file
```py
    if __name__ == '__main__':
        unittest.main()
```
If you include this on the bottom of the file, you will be able to run the file normally in IDEs and by calling
`python test_<filename>.py`

## setUp and tearDown
These methods allow for you to set up and tear down some data in every test in a file before and after each test runs
```py
    def setUp(self):
        # put test data in here, kinda like Dr.Kart's tests
        # When you use these add a 'self.' in front of all variables in it and again when referencing it
        # e.g. 
        #   self.x = 10
        #   ...
        #   print(self.x)
    
    def tearDown(self):
        # anything that needs to be done before the next test gets ran can be put here
```
There are also setUp and tearDown methods for the test suite as a whole
```py
    @classmethod
    setUpClass(cls):
        # This will run once at the beginning of all tests
    @classmethod
    tearDownClass(cls):
        # This will run once at the end of all tests
```

## Mocking
Mocking things up allows us to test if something is working with something that may not be in our control, or may not work yet (think websites)
```py
    from unittest.mock import patch
    
    ...
    
    def test_sample_test(self):
        with patch('<thing.to.simulate>') as mocked_thing:
            mocked_thing.return_value.attribute = <Value you want to simulate>
            # Put tests here
            mocked_thing.assert_called_with('<Value you want to check was passed to the thing>')
```


# Version Control System Docs and Online Resources

[Back to Home](../README.md)

**Prior** to using the information here, you should have a registered student account with Atlassian Bitbucket using your St. Edward’s email. If you haven’t already setup your student developer account, follow the instructions provided on the [developer setup guide](../README.md)

In this guide, we take a look at version control systems by exploring Git, which is an open-source version control system designed to handle larger software projects with multiple software engineers. Since Bitbucket, which is a UI repository service for hosting version control projects, will be used by your team in this course, we’ll inspect it as well.

## Creating a repository on your Bitbucket account

Login to your Bitbucket account. On the left sidebar, there should be a plus icon button. Click on this plus icon, and then select the create repository option.

A code **repository** is similar to a remote database which stores the metadata and source code associate with our project. Bitbucket requires that each repository have a workspace and a project. **Projects** are containers for repositories and **workspaces** are containers for both projects and repositories. Projects are useful when you want to group similar repositories together or grant the same permission levels to all repositories. Workspaces can be used in bitbucket to store multiple projects.

For the workspace option, select your default user workspace. For the project option, create a new project. Title it `BootcampProject`. You can name the repository anything you would like, but remember that naming conventions in Software Engineering should always be pneumonic - or indicative of what their purpose is to the casual observer. You should add `README.md` files to all of your repositories in this course as these are markdown files outlining the purpose of the source code in the repository. I.E., some questions to considering addressing on your `README.md` would be,

- What is being developed and tested in this repository?
- Who does this repository belong to?
- How does the source code in this repository relate to other repositorires in this project?
- How can I setup my local development environment so that I can start contributing to this repository?
- What has already been implemented in this repository and what's currently being built? (Integration Timeline)

![Create repository screenshot](https://i.imgur.com/SIvDl7I.png)

## Adding new users to your private repository

Navigate to the repository homepage you created on your Bitbucket account. You can do this by clicking the Rectangle Bitbucket icon logo on the top left of your screen, then click on the repositories option on the left sidebar. You should then get a list of repositories you’re authenticated to. Click on the `PythonAndVCSBootcamp` repository to go the homepage.

![Repository homepage screenshot](https://i.imgur.com/SqsTijb.png)

Next, click on the repository settings option on the left sidebar. You should now see a different list of options on the left sidebar.

Click on the `User and group access` option. This is where you need to grant access to users in your repository. One the “Users” toolbar, first make sure that the permission level is set to either “Write” or “Admin”, and not “Read” as it’s set by default when you add a new user.

![User and group access screenshot](https://i.imgur.com/DvbQCwt.png)

## Cloning the repository onto your system and setting up Git configuration

**Cloning** a repository refers to downloading the version control project onto your local system. In this case, since we’re using **Git**, cloning will download the Git repository associated with `PythonAndVCSBootcamp` onto your workstation.

There are different ways you can clone a repository – usually done through the SSH or HTTPS protocol. SSH cloning is generally more secure than HTTPS cloning as repository administrators can easily manage which repository users have access to write to the repository. However, in this course, we can use HTTPS cloning and it will work fine.

Go ahead and clone your repository onto your system using HTTPS. You can find the exact command to execute for this on your Bitbucket repository homepage.

You should also setup your git configuration username and email in the repository you just cloned. The `config` command with Git will set values on your username and email associated with your Bitbucket account for authentication purposes.

[Here's](https://git-scm.com/book/en/v2/Getting-Started-First-Time-Git-Setup) a nice reference for doing this.

## Creating a new version control branch

A **branch** is a very important concept in version control. Branches represent fully separated lines of development. Anytime you want to add new features and changes to existing code which has been written by your teammates, you should first create a new branch, which will point to an independent snapshot of the changes you’ve made without affecting any source code in other branches of the repository.

[Here’s](https://www.atlassian.com/git/tutorials/using-branches#:~:text=A%20branch%20represents%20an%20independent%20line%20of%20development.&text=The%20git%20branch%20command%20lets,checkout%20and%20git%20merge%20commands.) a nice article which details the basic concepts of git branching, as well as listing some useful commands you’ll need to create your own branch. If you’re more of a visual learner then [here’s](https://www.youtube.com/watch?v=FyAAIHHClqI&ab_channel=DavidMahler) a nice video detailing the git branching concept.

The command you need to create a new _local_ development branch is:
`git checkout -b <branchName>`

## Stage your new commits, commit them, and push them; seting up a remote tracking branch

![Git chart diagram](https://i.imgur.com/PHZiJ3i.gif)

Once you’ve finished writing your code, you’ll need to **add, commit, and push** your changes to your remote repository branch.

Committing and pushing code is how we share and publish our feature changes with teammates. Here’s a nice [video](https://www.youtube.com/watch?v=5HLst694D_Y&ab_channel=JamesStaab) which walks through how to add, commit, and push your new code to the remote repository.

_Tip_:
Keep in mind that there's a difference between a **local** and **remote** branch in Git. A local branch is a development branch that only you can see on your machine. A remote branch is a branch existing in the remote repository (to view the repository upstream URL, use the command `git remote -v`).

Here's a [thread](https://stackoverflow.com/questions/16408300/what-are-the-differences-between-local-branch-local-tracking-branch-remote-bra/24785777) you'll need to read on setting up a **remote tracking branch**, which is a local copy of a remote branch.

![Git workflow sequence](https://i.imgur.com/f2xUmDV.png)

## Fetching and pulling in commits from teammates

After your teammate has finished pushing their new code and branch to your remote repository, you’ll now need to `git fetch` the new remote branch created by your teammate onto your system and pull the new source code onto your system. **Fetching** in Git will download all remote commits, files, and branches to your local repository. After you fetch, you should see a new branch `UtilsImpl`. A ` git pull` will `fetch` the new remote repository commits while also _merging_ the code onto your local repository head at the same time.

_Hint: Use the `git checkout <branchName>` command to switch between different branches on your system. The command `git branch -a` will list out all branches for you to view._

Here are some nice references to help you if you get stuck here:

- https://www.atlassian.com/git/tutorials/syncing/git-pull
- https://www.atlassian.com/git/tutorials/using-branches/git-checkout

## Submitting pull requests on Bitbucket (mandatory)

A **pull request** is how we request to combine (or `merge`) our code in our branch with existing code in another upstream branch. Pull requests are a chance for your other team members to view the changes you’ve made in your feature branch and discuss any updates that may need to made to your code before merging.

Follow this [Bitbucket guide](https://support.atlassian.com/bitbucket-cloud/docs/create-a-pull-request-to-merge-your-change/) for the steps on how to create a pull request on Bitbucket.

_Tip: Before you publish a new pull request, it’s always a good idea to review the diffs on Bitbucket so that you can easily review your own work and correct any mistakes if need be. The diffs screen looks similar to this on Bitbucket UI (and will be shown to you by default when you publish the pull request and to the TA’s you’re adding to the request)._

![Diff screenshot](https://i.imgur.com/pMH6Njb.png)

## Module cleanup

- To delete a remote branch on Bitbucket (After merging UtilsImpl to master, there would be no need to use the UtilsImpl branch in this repository any longer):
  1. Go to your repository homepage on Bitbucket
  2. Click on the ‘Branches’ tab in the left sidebar
  3. In the actions column there should be an ellipsis for each branch. Click on the ellipsis for the branch you want to delete and select the delete dropdown option for that branch
- To delete a local branch on your system: https://stackoverflow.com/questions/15150671/delete-branches-in-bitbucket
- To delete a repository on bitbucket: https://support.atlassian.com/bitbucket-cloud/docs/delete-a-repository/
